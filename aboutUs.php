<?php 
include ('header.php');
?>

        <section class="contentWrapper">
            <!-- [LEFT SIDE MENU] -->
            <aside>
                <nav class="menuBar">
                    <li><a href="#">НОВИНКИ</a></li>
                    <li><a href="#">ЛУЧШИЕ ПРЕДЛОЖЕНИЯ</a></li>
                    <li><a href="#">РАСПРОДАЖА</a></li>

                    <li><a href="#">Шапки</a></li>
                    <li>
                        <a href="#" class="menuBar__more">Шарфы и платки</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Солнцезащитные платки</a></li>
                    <li><a href="#">Ремни</a></li>
                    <li><a href="#">Часы</a></li>
                    <li><a href="#">Кошельки</a></li>
                    <li><a href="#">Перчатки</a></li>
                    <li><a href="#">Зонты</a></li>
                    <li>
                        <a href="#" class="menuBar__more">Для волос</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menuBar__more">Украшения</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menuBar__more">Пляжные аксессуары</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Носки</a></li>
                    <li><a href="#">Домашняя обувь</a></li>
                    <li><a href="#">Маски карнавальные</a></li>

                    <li>
                        <a href="#" class="menuBar__more">ДЕТЯМ</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menuBar__more">МУЖЧИНАМ</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                </nav>
            </aside>
            <!-- [/END MENU] -->

            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">О нас</a></li>
                </nav>
                <h1>О нас</h1>
                <!-- [/End Head] -->

                <!-- [MAIN CONTENT] -->
                <main class="aboutPage">
                    <img src="img/aboutUs.jpg" alt="">
                    <span>
                        Aксессуары Frimis созданы специально для девушек и женщин, стремящихся следовать модным тенденциям.<br>
                        Frimis - это широкий выбор бижутерии, аксессуаров, украшений для волос.
                    </span>

                    <section class="aboutPage__wrapper">
                        <figure>
                            <img src="img/about1.jpg" alt="">
                            <figcaption>Опыт работы с 2007 года</figcaption>
                        </figure>
                        <figure>
                            <img src="img/about2.jpg" alt="">
                            <figcaption>Регулярные скидки, акции, распродажи, накопительная бонусная система</figcaption>
                        </figure>
                        <figure>
                            <img src="img/about3.jpg" alt="">
                            <figcaption>Новинки поступления<br>каждую неделю</figcaption>
                        </figure>
                    </section>
                </main>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    
    <!-- [FOOTER] -->
    <footer>
        <div class="footer__wrapper">
            <section class="footer__top">
                <nav>
                    <li><a href="#">Как заказать</a></li>
                    <li><a href="#">Бонусная программа</a></li>
                    <li><a href="#">Оплата и доставка</a></li>
                    <li><a href="#">Гарантии и возврат</a></li>
                    <li><a href="#">Вопрос-ответ</a></li>
                </nav>
                <nav>
                    <li><a href="#">О компании</a></li>
                    <li><a href="#">Отзывы</a></li>
                    <li><a href="#">Франшиза</a></li>
                    <li><a href="#">Контакты</a></li>
                </nav>
                <section class="footer__contacts">
                    <ul class="number">
                        <li><i class="fas fa-phone-alt"></i>8-888-888-88-88</li>
                        <li><i class="fas fa-envelope"></i>frimis@gmail.com</li>
                    </ul>
                    <ul class="social">
                        <li><a href="#"><i class="fab fa-vk"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-odnoklassniki"></i></a></li>
                    </ul>
                </section>
                <form action="#">
                    <h4>Оформите подписку</h4>
                    <input type="text" placeholder="Укажите e-mail">
                    <input type="submit" value="Подписаться">
                    <label>
                        Нажимая на кнопку «Подписаться», я
                        соглашаюсь на обработку моих персональных
                        данных и ознакомлен(а) с условиями
                        конфиденциальности.
                    </label>
                </form>
            </section>
            <section class="footer__info">
                <span>
                    © «Frimis» — интернет-магазин украшений и аксессуаров.<br>
                    <a href="#">Политика конфиденциальности.</a>
                </span>
                <a href="#" class="fiveLogo">Разработка<br>и дизайн сайта «FIVE»</a>
            </section>
        </input>
    </footer>
    <!-- [/END FOOTER] -->

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>

    <script src="js/main.js"></script>
    <!-- [/SCRIPTS] -->
</body>
</html>